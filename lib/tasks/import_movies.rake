namespace :csv do
  desc 'seed database with movies from csv'
  task :import => :environment do
    File.new(Rails.root.join('public', 'movies.csv'), 'r').read.split(/\r/).each_with_index do |line,index|
      puts 'Processing entry number ' + (index+1).to_s
      Movie.create(themoviedb: line.split(';')[1], title: line.split(';')[2]) if line.split(';').size > 2
    end
  end
end
