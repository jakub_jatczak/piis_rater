namespace :csv do
  desc 'export movies evaluations from db'
  task :export => :environment do

    # Person
    # ID  INT Person identifier
    # Description STRING(50)  Unique description (name and surname, nickname, etc.)
    # Evaluation
    # ID  INT Evaluation identifier
    # PersonID  INT Person identifier from Person table
    # MovieID INT Movie identifier from Movie table
    # Evaluation  INT Evaluation
    
    # People that voted
    people_file = File.new(Rails.root.join('public', 'persons.csv'), 'w')
    Voter.all.load.each do |voter|
      puts "#{voter.name}: #{voter.evaluations.count}#{', adding to list of people who voted' if voter.evaluations.count == Movie.all.count}"
      if voter.evaluations.count == Movie.all.count
        people_file.write "#{voter.id};#{voter.name}\n"        
      end
    end
    people_file.close

    # Evaluations
    evals_file = File.new(Rails.root.join('public', 'evaluations.csv'), 'w')
    Evaluation.all.load.each do |eval|
      puts "#{eval.id}, adding to list of evaluations"
      evals_file.write "#{eval.id};#{eval.voter_id};#{eval.movie_id};#{eval.rating.nil? ? 'NULL' : eval.rating}\n"
    end
    evals_file.close    
  end
end
