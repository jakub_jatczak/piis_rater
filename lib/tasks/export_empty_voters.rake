namespace :csv do
  desc 'export empty voters'
  task :empty_users => :environment do
    # People that registered but have not voted
    people_file = File.new(Rails.root.join('public', 'empty_voters.csv'), 'w')
    Voter.all.load.each do |voter|
      puts "#{voter.name}: #{voter.evaluations.count}#{', adding to list of people who skipped voting' if voter.evaluations.count == 0}"
      if voter.evaluations.count == 0
        people_file.write "#{voter.id};#{voter.name}\n"        
      end
    end
    people_file.close
  end
end
