class CreateEvaluations < ActiveRecord::Migration
  def change
    create_table :evaluations do |t|
      t.integer :rating
      t.integer :movie_id
      t.integer :voter_id
      t.timestamps
    end
  end
end
