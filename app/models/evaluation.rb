class Evaluation < ActiveRecord::Base
  validate :movie_id, :voter_id, presence: true
  validate :rating, numericality: {only_integer: true, greater_than: -1, less_than: 6}
  belongs_to :voter
  belongs_to :movie
  scope :not_voted, ->{where(rating: nil)}
end
