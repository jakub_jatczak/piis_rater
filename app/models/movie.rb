class Movie < ActiveRecord::Base
  validate :themoviedb, :title, presence: true
  default_scope ->{order(:created_at)}
  has_many :evaluations
end
