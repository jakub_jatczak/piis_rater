class Voter < ActiveRecord::Base
  validates :name, presence: true
  validates :name, uniqueness: true
  default_scope ->{order(:created_at)}
  has_many :evaluations, dependent: :destroy
end
