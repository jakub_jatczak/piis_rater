class EvaluateMoviesController < ApplicationController
  def evaluate_movies
    @voter = Voter.find(cookies.signed[:voter_id])
    Movie.all.each do |m|
      @voter.evaluations.new(movie_id: m.id, voter_id: @voter.id)
    end
  end

  def save_evaluations
    puts params
    params[:evaluation].each do |evaluation|
      @eval = Evaluation.new()
      @eval.voter_id = evaluation[1][:voter_id].to_i
      @eval.movie_id = evaluation[1][:movie_id].to_i
      evaluation[1][:rating] != '-1' ? @eval.rating = evaluation[1][:rating] : @eval.rating = nil
      @eval.save
    end
    cookies.delete :voter_id
    redirect_to thank_you_path
  end
end
