class StaticController < ApplicationController
  def index; end

  def thank_you; end

  def stats
    @voters_count=Voter.count
    @voters_finished=Evaluation.count/Movie.count
    @voters_failed=Voter.count-(Evaluation.count/Movie.count)

    @total_evals=Evaluation.count
    @avg_seen_per_session=Evaluation.where.not(rating: nil).count/(Evaluation.count/Movie.count)
    @avg_rating = 0
    Evaluation.where.not(rating: nil).each do |ev|
      @avg_rating += ev.rating
    end
    @avg_rating /= Evaluation.where.not(rating: nil).count
  end
end
