class TmdbController < ApplicationController
  layout false
  def show
    @tmdb_id = Movie.find(params[:id]).themoviedb
    @movie_params = Tmdb::Movie.detail(@tmdb_id)
    @base_url = Tmdb::Configuration.new.base_url
  end
end
