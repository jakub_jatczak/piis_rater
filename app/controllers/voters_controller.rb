class VotersController < ApplicationController
  def new
    @voter = Voter.new
  end

  def create
    @voter = Voter.new(voter_params)
    if @voter.save
      cookies.signed[:voter_id] = @voter.id
      redirect_to evaluate_movies_path
    else
      render :new
    end
  end

  def update
    @voter.find(params[:id])
    @voter.update(voter_params)
    redirect_to thanks_path
  end

  private

  def voter_params
    params.require(:voter).permit(:name, evaluations_attributes: [:rating])
  end
end
