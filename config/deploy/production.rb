role :app, %w{185.49.14.173}
role :web, %w{185.49.14.173}
role :db,  %w{185.49.14.173}

server '185.49.14.173', user: 'rater', roles: %w{web app db}

set :rails_env, 'production'
set :branch, "master"
set :full_app_name, 'rater'
# set :applicationdir,  "/home/#{fetch(:full_app_name)}/www/"
set :deploy_to,  "/home/#{fetch(:full_app_name)}/www/"
# set :deploy_to,       "#{fetch(:applicationdir)}"
