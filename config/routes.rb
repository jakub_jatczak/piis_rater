Rails.application.routes.draw do
  root 'static#index'
  resources :voters, only: [:new, :create, :update]
  get '/evaluate_movies', to: 'evaluate_movies#evaluate_movies', as: :evaluate_movies
  post '/save_evaluations', to: 'evaluate_movies#save_evaluations', as: :save_evaluations
  get '/thank_you', to: 'static#thank_you', as: :thank_you
  get '/tmdb/show/:id', to: 'tmdb#show', as: :tmbd
  get '/stats', to: 'static#stats', as: :stats
end
